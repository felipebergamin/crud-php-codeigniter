<?php

class ContatoModel extends CI_Model {
  private $tableName;

  public function __construct(){
    $this->load->database();

    $this->tableName = 'contatos';
  }

  public function countAll() {
    return $this->db->count_all($this->tableName);
  }

  public function get($id) {
    if (isset($id)) {
      return $this->db->get_where($this->tableName, array('id' => $id))->row_array();
    }

    return null;
  }

  public function update($id, $data) {
    $this->db->where('id', $id);
    $this->db->update($this->tableName, $data);
  }

  public function listAll(){
    $query = $this->db->get('contatos');
    return $query->result_array();
  }

  public function list($limit, $offset) {
    $this->db->limit($limit, $offset);
    $query = $this->db->get($this->tableName);
    return $query->result_array();
  }

  public function create(){
    $data = array(
      'nome' => $this->input->post('nome'),
      'nascimento' => $this->input->post('nascimento'),
      'email' => $this->input->post('email'),
      'sexo' => $this->input->post('sexo'),
    );

    return $this->db->insert('contatos', $data);
  }

  public function delete($id) {
    $this->db->delete('contatos', array('id' => $id));
  }
}
