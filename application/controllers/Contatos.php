<?php

class Contatos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('ContatoModel');
    $this->load->helper('url');
  }

  public function listarJson(){
    $data = $this->ContatoModel->listAll();
    header('Content-Type: application/json');
    echo json_encode($data);
  }

  public function datatable(){
    $this->load->view('templates/header', array('pageTitle' => 'Datatable de Contatos'));
    $this->load->view('contatos/datatable');
    $this->load->view('templates/footer');
  }

  public function listar($page = 0){
    $rowPerPage = 2;

    $this->load->library('pagination');
    $config['base_url'] = base_url() . '/contatos/listar';
    $config['total_rows'] = $this->ContatoModel->countAll();
    $config['per_page'] = $rowPerPage;
    $this->pagination->initialize($config);

    $viewData['contatos'] = $this->ContatoModel->list($rowPerPage, $page);
    $viewData['insertMessage'] = filter_var($this->input->get('insert'), FILTER_VALIDATE_BOOLEAN);
    $viewData['deleteMessage'] = filter_var($this->input->get('delete'), FILTER_VALIDATE_BOOLEAN);
    $viewData['paginationLinks'] = $this->pagination->create_links();
    
    $this->load->view('templates/header', array('pageTitle' => 'Lista de Contatos'));
    $this->load->view('contatos/listar', $viewData);
    $this->load->view('templates/footer');
  }

  public function criar(){
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('nome', 'Nome', 'required');
    $this->form_validation->set_rules('nascimento', 'Nascimento', 'required');
    $this->form_validation->set_rules('email', 'email', 'required|valid_email');
    $this->form_validation->set_rules('sexo', 'Sexo', 'required');

    if ($this->form_validation->run() === false) {
      $this->load->view('templates/header', array('pageTitle' => 'Novo Contato'));
      $this->load->view('contatos/criar');
      $this->load->view('templates/footer');
    } else {
      // criar model no bd
      $this->ContatoModel->create();
      redirect('/contatos/listar?insert=true');
    }
  }

  public function excluir($id) {
    if (isset($id) && is_numeric($id)) {
      $this->ContatoModel->delete($id);
      redirect('/contatos/listar?delete=true');
    }
  }

  public function editar($id) {
    $contato = $this->ContatoModel->get($id);

    if ($contato === null) {
      show_404();
      return;
    }

    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('nome', 'Nome', 'required');
    $this->form_validation->set_rules('nascimento', 'Nascimento', 'required');
    $this->form_validation->set_rules('email', 'email', 'required|valid_email');
    $this->form_validation->set_rules('sexo', 'Sexo', 'required');

    if ($this->form_validation->run() === false) {
      $this->load->view('templates/header', array('pageTitle' => 'Editar Contato'));
      $this->load->view('contatos/criar', array('contato' => $contato));
      $this->load->view('templates/footer');
    } else {
      $data = array(
        'nome' => $this->input->post('nome'),
        'nascimento' => $this->input->post('nascimento'),
        'email' => $this->input->post('email'),
        'sexo' => $this->input->post('sexo')
      );

      $this->ContatoModel->update($id, $data);
      redirect('contatos/listar?update=true');
    }
  }
}
