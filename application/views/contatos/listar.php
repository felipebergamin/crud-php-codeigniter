
<div class="container">
  <h1>Lista de Contatos</h1>

  <a href="<?=base_url('contatos/criar')?>" class="btn btn-primary">Novo</a>

  <?php if(isset($insertMessage) && $insertMessage === true): ?>
    <div class="alert alert-success" role="alert">
      Contato adicionado com sucesso!
    </div>
  <?php endif; ?>

  <?php if(isset($deleteMessage) && $deleteMessage === true): ?>
    <div class="alert alert-success" role="alert">
      Contato deletado!
    </div>
  <?php endif; ?>

  <table class='table'>
    <thead>
      <th>Nome</th>
      <th>Nascimento</th>
      <th>E-mail</th>
      <th>Sexo</th>
      <th></th>
    </thead>

    <?php foreach($contatos as $contato): ?>
      <tr>
        <td><?=$contato['nome']?></td>
        <td><?=$contato['nascimento']?></td>
        <td><?=$contato['email']?></td>
        <td><?=$contato['sexo']?></td>
        <td>
          <a class="btn btn-outline-danger" href='<?=base_url("/contatos/excluir/$contato[id]")?>'>Excluir</a>
          <a class="btn btn-outline-dark" href='<?=base_url("/contatos/editar/$contato[id]")?>'>Editar</a>
        </td>
      </tr>
    <?php endforeach; ?>

  </table>

  <?=$paginationLinks?>
</div>
