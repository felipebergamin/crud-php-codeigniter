<div class="container">
  <h3><?= isset($contato) ? 'Editar Contato' : 'Novo Contato' ?></h3>

  <?=form_open(isset($contato) ? "contatos/editar/$contato[id]" : "contatos/criar")?>
    <div class="form-group">
      <label for="nome">Nome</label>
      <input
        class="form-control"
        type="text"
        name="nome"
        id="nome"
        required
        value="<?= isset($contato) ? $contato['nome'] : '' ?>">
    </div>

    <div class="form-group">
      <label for="nascimento">Nascimento</label>
      <input
        class="form-control"
        type="date"
        name="nascimento"
        id="nascimento"
        required
        value="<?= isset($contato) ? $contato['nascimento'] : '' ?>">
    </div>

    <div class="form-group">
      <label for="email">E-mail</label>
      <input
        class="form-control"
        type="email"
        name="email"
        id="email"
        required
        value="<?= isset($contato) ? $contato['email'] : '' ?>">
    </div>

    <div class="form-group">
      <label for="sexo">Example select</label>
      <select class="form-control" name="sexo" id="sexo" required>
        <option <?=isset($contato) ? '' : 'selected'?> disabled hidden>Selecione</option>
        <option value='masculino' <?= isset($contato) && $contato['sexo'] === 'masculino' ? 'selected="selected"' : '' ?>>Masculino</option>
        <option value='feminino' <?= isset($contato) && $contato['sexo'] === 'feminino' ? 'selected="selected"' : '' ?>>Feminino</option>
      </select>
    </div>

    <button type="submit" class="btn btn-primary">Enviar</button>
  </form>
</div>
