<div class="container">
  <table id="dataTable" class="display">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Nascimento</th>
        <th>E-mail</th>
        <th>Sexo</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Row 1 Data 1</td>
        <td>Row 1 Data 2</td>
      </tr>
      <tr>
        <td>Row 2 Data 1</td>
        <td>Row 2 Data 2</td>
      </tr>
    </tbody>
  </table>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script>
  $(document).ready(function() {
    $('#dataTable').DataTable({
      ajax: '/api/contatos/listar',
      dataType: 'json',
      sAjaxDataProp: '',
      columns: [
        { data: 'nome' },
        { data: 'nascimento' },
        { data: 'email' },
        { data: 'sexo' },
      ]
    });
  });
</script>