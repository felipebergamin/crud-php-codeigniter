DROP DATABASE IF EXISTS testefelipe;
CREATE DATABASE testefelipe;
USE testefelipe;

CREATE TABLE contatos(
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(128) NOT NULL,
  nascimento DATE NOT NULL,
  email VARCHAR(128) NOT NULL,
  sexo ENUM('masculino', 'feminino'),
  PRIMARY KEY (id)
);

INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Joao da Silva', '1995-02-10', 'joao@email.com', 'masculino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Maria Maria', '1990-02-12', 'maria@email.com', 'feminino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Silvano Silva', '1970-04-20', 'silvano@email.com', 'masculino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Ciclano', '1996-05-29', 'ciclano@email.com', 'masculino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Silvana Marta', '1960-01-10', 'silvana@email.com', 'feminino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Jurema', '1950-10-12', 'jurema@email.com', 'feminino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Edson', '1940-01-21', 'edson@email.com', 'masculino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Marta', '1967-02-28', 'marta@email.com', 'feminino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Cleonice Cleonice', '1988-08-08', 'cleonice@email.com', 'feminino');
INSERT INTO contatos(nome, nascimento, email, sexo) VALUES('Roberta', '1975-04-16', 'roberta@email.com', 'feminino');